# Changelog

### 0.4.0
* Improved compatibility to [Ham Galaxy Settings](https://www.avorion.net/forum/index.php/topic,5056.0.html) mod.
* Improved/fixed calculations of the damage the Dreadnought deals. This may affect difficulty. You should reconfigure it for your galaxy. Watch comments in config file.
* Added the Xsotan upscale function that came with Avorion 0.18.0 to scale the Dreadnought (can be disabled in config file).


### 0.3.0
* Moved project to GitLab