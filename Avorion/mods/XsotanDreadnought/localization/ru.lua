return {
	["Your sensors picked up very curious subspace signals at \\s(%i:%i)."] = "Ваши датчики обнаружили очень странные подпространственные сигналы в секторе \\s(%i:%i).",
	["Your sensors picked up subspace signals at %i:%i."] = "Ваши датчики обнаружили подпространственные сигналы в секторе %i:%i.",
	["Curious subspace signals"] = "Странные подпространственные сигналы",
	["You received curious subspace signals by an unknown source. Their position is %i:%i."] = "Вы получили странные подпространственные сигналы из неизвестного источника. Они указывают на сектор %i:%i.",
	["The Dreadnought charges up his weapons and shields"] = "Дредноут заряжает орудия и щиты",
	["The Dreadnought finished charging and is vulnerable again"] = "Дредноут закончил зарядку и снова уязвим",
}