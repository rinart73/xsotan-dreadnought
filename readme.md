# Xsotan Dreadnought

The Xsotan Dreadnought is a boss with very strong shields. It can charge up during fight wich repairs its shields and increase the damage output. Also he will call more xsotan ships to help him.

He got some kind of special shields that absorbs ionized projectils, shield breaking weapons and even collision damage.

## Installation instructions
### Step 1 - Installation
Download the XsotanDreadnought zip file and extract the content of its Avorion directory into your local Avorion directory.
### Step 2 - Initialization
Implement the event in your scripts. There are different methods to do this.

##### Random event

Implement mod as a random event that will be automaticly started about every 2-3 hours for users (like the distress call mission). Open the zip file and extract the content of its XDEvent directory into your local Avorion directory.

##### Mission at military outpost

Use this mod as a plugin for [MilitaryMissions mod](https://www.avorion.net/forum/index.php/topic,4903.0.html). Follow installation instructions of the MilitaryMissions mod and enable the Xsotan Dreadnought in the file `mods/MilitaryMissions/config/MilitaryMissionsConfig.lua`.

##### Custom initialization (for modders)

Alternatively you can use this mod as a resource that can be implemented in your own mods/scripts. F.e. you could add this as a mission at military station.

`Player():addScript("mods/XsotanDreadnought/scripts/player/XsotanDreadnoughtAttack.lua", true)`
or
`Player():addScriptOnce("mods/XsotanDreadnought/scripts/player/XsotanDreadnoughtAttack.lua", true)`

Calling this will start the event for the player.

### Step 3 - Translations (optional)
If you do not want to use translations for this mod or only need it in english, you may skip this step.
Support for i18n translation mod by rinart73 is implemented in this mod since version 0.1.2.

Included languages:

- English
- German



To enable translations download i18n mod and follow installation instructions (only "Installation", you must not do the steps "For users" and below).

[i18n Mod - forum topic](http://www.avorion.net/forum/index.php/topic,4330.0.html)

You can add additional languages, just take a look into `mods/XsotanDreadnought/localization/en.lua`

## Configuration
You should take a look into the config file:
`mods/XsotanDreadnought/config/XsotanDreadnoughtConfig.lua`

You can change a lot of balancing stuff there. The comments should say everything you need to know.

## Compatiblity
This mod is highly compatible with almost all other mods. It does not overwrite any game file.
Only the XDEvent (watch Installation step 2 for more information) overwrites the file eventscheduler.lua. If another mod already modifies your eventscheduler.lua, you have to merge them or implement the event trigger by yourself into any script.

For not making the Dreadnought stronger then the Wormhole Guardian you can also use the [Wormhole Scout mod](http://www.avorion.net/forum/index.php/topic,3614.0.html).

### Developing
For developing I used a modified entitydbg.lua to start the event, this file is also included in this package. This file may not be up to date cause I do not update it frequently, use at your own risk.


### Translations
You created your own language file for this mod, wich is not included in the current mod version, and want it to be released? Just send me your language.lua file.

### CarrierCommand priority
If you are using CarrierCommand mod by Laserzwei you may add a priority fighter use for the Dreanought. I recommand a priority of 15.

    Config.additionalPriorities = {
        xsotan_dreadnought = 15
    }